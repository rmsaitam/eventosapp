**Premissas:** Ter o Docker e Docker compose instalados.

1. Forkar e clonar o repositório
2. Acessar o diretório do repositório clonado
3. Executar na ordem

```
docker-compose up -d serverdbmysql

sudo docker-compose up java
```


No browser acessar http://localhost:8080/eventosapp

Feito!